package com.example.naoto.casestudyapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CalendarView;

public class SubmissionActivity extends AppCompatActivity {

    CalendarView calendarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submission);

        calendarView = (CalendarView) findViewById(R.id.calendarView);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_SUBJECT ,"[事例研究]発表資料提出締め切りについて");
                intent.putExtra(Intent.EXTRA_TEXT,
                        "○○研究室の皆様,\n" +
                        "\n" +
                        "○○研究室ゼミ長の○○です.\n" +
                        "\n" +
                        "事例研究発表の提出資料の締切日を設定いたしました．\n" +
                        "以下の日付までに提出してくださるようよろしくお願いいたします．\n" +
                        "\n" +
                        year + "年 " + (month + 1) + "月 " + dayOfMonth +"日"
                );
                try{
                    startActivity(Intent.createChooser(intent,"Send"));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }
}
