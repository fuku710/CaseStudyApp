package com.example.naoto.casestudyapp;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.FileInputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


public class TimetableFragment extends Fragment {

    private OnFragmentTimetableListener mListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_timetable, container, false);

        LinearLayout timetable = view.findViewById(R.id.timeTable);

        try{
            FileInputStream inputStream = getActivity().openFileInput("data.xml");

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(inputStream);

            CardView cardView;
            LinearLayout linearLayout;
            TextView startTimeText;
            TextView endTimeText;
            TextView userNameText;

            Node timetableNode = document.getElementsByTagName("Timetable").item(0);
            NodeList rowNodeList = document.getElementsByTagName("Row");
            Node rowNode = timetableNode.getFirstChild();
            int nodeLength = rowNodeList.getLength();

            for (int i = 0;i < nodeLength;i++) {
                final int index = i;

                rowNode = rowNodeList.item(i);
                NamedNodeMap attrs = rowNode.getAttributes();
                String startTime = attrs.getNamedItem("startTime").getTextContent();
                String endTime = attrs.getNamedItem("endTime").getTextContent();
                final String userName = rowNode.getTextContent();

                cardView = (CardView) getLayoutInflater().inflate(R.layout.view_card,null);

                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CardView cv = (CardView)v;
                        TextView userTime = (TextView) ((LinearLayout) ((LinearLayout) cv.getChildAt(0)).getChildAt(1)).getChildAt(1);
                        mListener.recordTime(userTime);
                    }
                });


                linearLayout = (LinearLayout) cardView.getChildAt(0);
                startTimeText = (TextView) linearLayout.getChildAt(0);
                userNameText = (TextView)((LinearLayout) linearLayout.getChildAt(1)).getChildAt(0);
                endTimeText = (TextView) linearLayout.getChildAt(2);
                startTimeText.setText(startTime);
                userNameText.setText(userName);
                endTimeText.setText(endTime);

                timetable.addView(cardView);

            }

        } catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentTimetableListener) {
            mListener = (OnFragmentTimetableListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public interface OnFragmentTimetableListener {
        void recordTime(TextView textView);
    }
}
